import asyncio
from autobahn.wamp.types import PublishOptions
import requests
import time
from collections import namedtuple
import traceback
from json import loads

from zephir.i18n import _

Event = namedtuple('Event', ['tag', 'data'])

class SaltEventsError(Exception):
    """Base class of :class:`SaltEvents` exceptions

    """

class SaltEventsLoginError(SaltEventsError):
    """Raised when an error occurs when login to salt-api

    """

class SaltEvents():
    """Listen to saltmaster events and emit forward them as WAMP messages

    """

    def __init__(self, config, wamp):
        self.config = config
        self.wamp = wamp

    def login(self):
        print(_('connecting to salt-api'))
        session = requests.Session()
        try:
            # TODO use provided configuration to get salt-api endpoint and credentials
            resp = session.post('http://localhost:8000/login',
                                json={'username': 'eole',
                                      'password': 'eole',
                                      'eauth': 'pam',
                                }, timeout=(3.05, 27))
            assert resp.status_code == 200, resp.reason
            return session
        except Exception as err:
            session.close()
            err_msg = _('Error while authenticating to salt-api : {}').format(err)
            raise SaltEventsLoginError(err_msg)

    def try_login(self):
        session = None
        while session is None:
            try:
                session = self.login()
            except Exception as err:
                delay = 5
                msg = _('could not authenticate to salt-api. retrying in {} seconds...').format(delay)
                print(msg)
                time.sleep(delay)
        return session

    def forward_events(self):
        while True:
            try:
                with self.try_login() as session:
                    print(_('connected to salt-api'))
                    res = session.get('http://localhost:8000/events', stream=True)
                    assert res.status_code == 200, res.reason
                    print('waiting for saltmaster events...')
                    buffer = ""
                    for line in res.iter_lines(decode_unicode=True):
                        buffer += line+'\n'
                        event, rest = self.parse(buffer)
                        if event != None:
                            self.dispatch_events(event)
                        buffer = rest
                    print('stream closed')
            except Exception as err:
                traceback.print_exc()
                err_msg = _('Error while streaming salt-master events : {}').format(err)
                print(err_msg)

    def dispatch_events(self, event):
        tag = event.tag
        if tag == 'minion_start':
            server_id = loads(event.data).get('data', {}).get('id')
            if server_id is None:
                return
            server_id = int(server_id)
            pub_options = PublishOptions(exclude_me=False)
            self.wamp.publish('v1.execution.salt.master.event.start', server_id=server_id, options=pub_options)
        if 'salt/job' in tag:
            server_id = loads(event.data).get('data', {}).get('id')
            datas = loads(event.data).get('data')
            if 'fun_args' in datas and 'success' in datas:
                if 'reconfigure' in datas['fun_args'] and datas['success'] == True:
                    self.wamp.publish('v1.execution.salt.master.event.ready', server_id=int(server_id))


    # Based on https://github.com/tOkeshu/eventsource-parser
    # Licensed under the Apache License, Version 2.0.
    def parse(self, source):
        tag = None
        data  = []
        retry = None
        extra = ''

        dispatch = False
        cursor   = 0
        lines    = source.splitlines()

        for line in lines:
            if not line:
                dispatch = True
                extra = source[cursor+1:]
                break

            if not ':' in line:
                field, value = line, ''
            else:
                field, value = line.split(':', 1)

            if value and value[0] == ' ':
                value = value[1:]

            if field == 'data':
                data.append(value)
            elif field == 'tag':
                tag = value
            elif field == 'retry':
                retry = int(value)

            cursor += len(line) + 1

        if not dispatch:
            return None, source
        if (tag, data, retry) == (None, [], None):
            return None, extra

        if data:
            data = '\n'.join(data)

        if retry:
            if tag or data:
                extra = (u'retry: %s\n\n%s' % (retry, extra))
            else:
                tag, data = 'retry', retry

        return Event(tag, data), extra
