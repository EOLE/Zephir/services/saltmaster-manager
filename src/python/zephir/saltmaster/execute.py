import asyncio
import requests

from zephir.i18n import _

class SaltExecError(Exception):
    """Base class of :class:`SaltExec` exceptions

    """


class SaltExecErrorMissingMinionPattern(SaltExecError):
    """Raised when a request does not provide the `minion_pattern` keywork argument

    """


class SaltExecErrorMissingCommand(SaltExecError):
    """Raised when a request does not provide the `command` keywork argument

    """


class SaltExecErrorLoginError(SaltExecError):
    """Raised when an error occurs when login to salt-api

    """



class SaltExecErrorExecuteError(SaltExecError):
    """Raised when an execution request do not return with an OK code

    """
    pass


class SaltExec():
    """Execute commands on SaltStack

    """

    def __init__(self, config):
        self.config = config


    def exec_command(self, minion_pattern, command, arg=None, client_mode='local_async', tgt_type='glob'):
        msg = _('Salt executor : shedule execution of “{}({})” on target “{}”')
        print(msg.format(command, arg or "", minion_pattern))
        session = requests.Session()
        try:
            resp = session.post('http://localhost:8000/login',
                                json={'username': 'eole',
                                      'password': 'eole',
                                      'eauth': 'pam'                                                                     
                                })

            assert resp.status_code == 200, resp.reason

        except Exception as err:
            err_msg = _('Error while authenticating to salt-api : {}').format(err)
            raise SaltExecErrorLoginError(err_msg)

        try:
            json_payload = {
                'client': client_mode,
                'tgt': minion_pattern,
                'fun': command,
                'tgt_type': tgt_type   
            }

            if arg is not None:
                json_payload['arg'] = [arg]

            resp = session.post('http://localhost:8000', json=[json_payload])
            assert resp.status_code == 200, resp.reason

            print(resp.json())
            ret = resp.json()['return'][0]
            print(ret)
            if not ret:
                raise SaltExecError(_('salt did not find any server to execute the command'))

            if client_mode == 'local_async':
                dico = {'jid': ret['jid'],
                        'minions': ret['minions']}
            elif client_mode == 'local':
                dico = ret
            else:
                raise Exception(_('unknown client_mode type {}').format(client_mode))
            return dico

        except Exception as err:
            err_msg = _('Error while executing command “{}({})” through salt-api : {}')
            raise SaltExecErrorExecuteError(err_msg.format(command, arg, err))
