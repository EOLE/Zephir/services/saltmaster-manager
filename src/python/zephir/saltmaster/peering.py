import asyncio
import aiohttp
import uuid
import re
import subprocess
import os
from Crypto.PublicKey import RSA

keys_re = re.compile(r'(?P<pub>-----BEGIN PUBLIC KEY-----.*-----END PUBLIC KEY-----).*(?P<priv>-----BEGIN RSA PRIVATE KEY-----.*-----END RSA PRIVATE KEY-----)', re.DOTALL)

async def get_keys(session, minion_id):
    async with session.post('http://localhost:8000/keys', data={'username': 'eole', 'password': 'eole', 'eauth': 'pam', 'mid': minion_id}) as response:
        result = await response.text()
    try:
        pub_key, priv_key = keys_re.search(str(result)).groups()
    except Exception as err:
        msg = 'Error retrieving keys from /keys api endpoint: {}'.format(str(err))
        raise SaltPeeringError(msg)
    return pub_key, priv_key


def get_master_fingerprint():
    try:
        master_fingerprint = re.search(r'.*master\.pub:\s*(?P<master_finger>[a-f0-9:]*)', subprocess.check_output(['salt-key', '-F', 'master']).decode()).group('master_finger')
        return master_fingerprint

    except Exception as err:
        err_msg = 'Error while getting master fingerprint'
        raise SaltPeeringError(err_msg)


def is_vault_available(self):
    return self.config.option['vault'].get('host') is not None


class SaltPeeringError(Exception):
    """Base class of :class:`SaltSubscribe` exceptions

    """


class SaltPeeringLoginError(SaltPeeringError):
    """Base class of :class:`SaltSubscribe` exceptions

    """


class SaltPeeringUIDDuplicateError(SaltPeeringError):
    """Base class of :class:`SaltSubscribe` exceptions

    """


class SaltPeering():
    """Handle minion subscription

    """

    def __init__(self, config):
        self.config = config
        self.session = None

    async def login(self, username=None, password=None, **kwargs):
        self.session = aiohttp.ClientSession()
        if username is None:
            err_msg = 'username needed'
            raise SaltPeeringError(err_msg)
        if password is None:
            err_msg = 'password needed'
            raise SaltPeeringError(err_msg)

        await self.session.post('http://localhost:8000/login', data={'username': username, 'password': password, 'eauth': 'pam'})


    async def generate_keys(self, uid=None, passphrase=None):
        """
        """
        if passphrase is None:
            raise SaltPeeringError()
        if uid is None:
            raise SaltPeeringError()
        try:
            async with aiohttp.ClientSession() as session:
                pub_key, priv_key = await get_keys(session, uid)
            RSA_PrivKey = RSA.importKey(priv_key)
            cipher_priv_key = RSA_PrivKey.exportKey(format='PEM', passphrase=passphrase.encode())
            return pub_key, cipher_priv_key
        except Exception as err:
            err_msg = 'Error while generating keys: {}'.format(str(err))
            raise SaltPeeringError(err_msg)


    async def generate_conf(self):
        try:
            master_fingerprint = get_master_fingerprint()
        except Exception as err:
            err_msg = 'Error while generating minion configuration'
            raise SaltPeeringError(err_msg)

        minion_conf = "master: {}\nmaster_port: {}\nhash_type: {}\nmaster_finger: {}\npillar_raise_on_missing: True\nminion_pillar_cache: True"
        return minion_conf.format(os.environ['SALTMASTER_PUBLIC_HOST'], 30010, 'sha256', master_fingerprint)
