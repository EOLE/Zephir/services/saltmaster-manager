####
#### Temporary layer to prepare installation
####
FROM ubuntu:bionic AS build

# Packages required to build
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get install -y \
    curl \
    git \
    gnupg

ARG CONTAINERPILOT_VERSION=3.4.3
ARG CONTAINERPILOT_CHECKSUM=e8258ed166bcb3de3e06638936dcc2cae32c7c58

RUN curl -Lso /tmp/containerpilot.tar.gz \
         "https://github.com/joyent/containerpilot/releases/download/${CONTAINERPILOT_VERSION}/containerpilot-${CONTAINERPILOT_VERSION}.tar.gz" \
    && echo "${CONTAINERPILOT_CHECKSUM}  /tmp/containerpilot.tar.gz" | sha1sum -c \
    && tar zxf /tmp/containerpilot.tar.gz -C /tmp

ARG CONSULTEMPLATE_VERSION=0.19.3
ARG CONSULTEMPLATE_CHECKSUM=6467b442d8fd68ca0a1eba35f42edb6f3b9941e10b6d58688edc3506fca0bb19

RUN curl -Lso /tmp/consul-template.tar.gz \
         "https://releases.hashicorp.com/consul-template/${CONSULTEMPLATE_VERSION}/consul-template_${CONSULTEMPLATE_VERSION}_linux_amd64.tgz" \
    && echo "${CONSULTEMPLATE_CHECKSUM}  /tmp/consul-template.tar.gz" | sha256sum -c \
    && tar zxf /tmp/consul-template.tar.gz -C /tmp


# Orcherstration using containerpilot
# Configuration using consul-template
ARG CONTAINERPILOT_REPO_URL=https://gitlab.mim.ovh/EOLE/Zephir/Services/orchestrate-containerpilot.git
ARG CONTAINERPILOT_REPO_REF=223dafd3d093702c8717292247e65d0c55ffb513

RUN git clone "${CONTAINERPILOT_REPO_URL}" "/tmp/orchestrate" \
    && cd /tmp/orchestrate \
    && git checkout "${CONTAINERPILOT_REPO_REF}"


# API messages description
ARG MESSAGES_API_REPO_URL=https://gitlab.mim.ovh/EOLE/Zephir/messages-api.git
ARG MESSAGES_API_REPO_REF=0.1.0

RUN git clone "${MESSAGES_API_REPO_URL}" "/tmp/messages-api" \
    && cd /tmp/messages-api \
    && git checkout "${MESSAGES_API_REPO_REF}"


# Common python Zéphir library
ARG PYTHON_ZEPHIR_REPO_URL=https://gitlab.mim.ovh/EOLE/Zephir/python-zephir.git
ARG PYTHON_ZEPHIR_REPO_REF=0.1.0

RUN git clone "${PYTHON_ZEPHIR_REPO_URL}" "/tmp/python-zephir" \
    && cd /tmp/python-zephir \
    && git checkout "${PYTHON_ZEPHIR_REPO_REF}"


##
## Service specific
##

# Configure package manager
RUN echo "deb http://repo.saltstack.com/apt/ubuntu/18.04/amd64/2018.3 bionic main" > /etc/apt/sources.list.d/saltstack.list
RUN curl https://repo.saltstack.com/apt/ubuntu/18.04/amd64/2018.3/SALTSTACK-GPG-KEY.pub | apt-key --keyring /etc/apt/trusted.gpg.d/saltstack.gpg add -

# SaltStack formulas parameters
ENV EOLE_FORMULA_DIR=/srv/formulas

ENV EOLE_CONFIGURATION_FORMULA_URL="https://dev-eole.ac-dijon.fr/git/eole-configuration-formula.git"
ENV EOLE_CONFIGURATION_FORMULA_REF=develop

RUN git clone "${EOLE_CONFIGURATION_FORMULA_URL}" "${EOLE_FORMULA_DIR}/eole-configuration-formula" \
    && cd "${EOLE_FORMULA_DIR}/eole-configuration-formula" \
    && git checkout "${EOLE_CONFIGURATION_FORMULA_REF}"


####
#### Target layer
####
FROM ubuntu:bionic

MAINTAINER Pôle EOLE <eole@ac-dijon.fr>

# Packages required for working service
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get install -y \
    gnupg \
    jq \
    locales \
    openssl \
    python3 \
    python3-aiohttp \
    python3-autobahn \
    python3-dev \
    python3-jwt \
    python3-psycopg2 \
    python3-requests \
    python3-yaml \
    python3-pip \
    sqitch \
    tzdata

# For debug only
RUN apt-get update -y && apt-get install -y \
    curl \
    iproute2 \
    iputils-ping \
    vim

# Force french locale
RUN locale-gen fr_FR.UTF-8
ENV LANG fr_FR.UTF-8
ENV LC_ALL fr_FR.UTF-8

# Force timezone
RUN ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata

RUN pip3 install casbin

# Sqitch
RUN sqitch config --user user.name 'Equipe EOLE'\
    && sqitch config --user user.email 'eole@ac-dijon.fr'

# Install tools from build layer
COPY --from=build /tmp/containerpilot /usr/local/bin
COPY --from=build /tmp/consul-template /usr/local/bin

# consul-template wrapper to generate /etc/zephir-services.conf
COPY --from=build /tmp/orchestrate/update-service-conf /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# Service template for consul-template
ENV services_conf_filename=zephir-services.conf
ARG services_conf=/etc/zephir-services.conf
ENV services_conf=$services_conf
COPY --from=build /tmp/orchestrate/${services_conf_filename}.ctmpl ${services_conf}.ctmpl

# Install libraries required by service
COPY --from=build /tmp/python-zephir/zephir /usr/lib/python3/dist-packages/zephir
COPY --from=build /tmp/python-zephir/acl /etc/acl
COPY --from=build /tmp/messages-api/messages /srv/messages

# Manage container with ContainerPilot
CMD ["/usr/local/bin/containerpilot", "-config", "/etc/containerpilot.json5"]
COPY containerpilot.json5 /etc/containerpilot.json5


##
## Service specific
##

COPY --from=build /etc/apt/sources.list.d/saltstack.list /etc/apt/sources.list.d/saltstack.list
COPY --from=build /etc/apt/trusted.gpg.d/saltstack.gpg /etc/apt/trusted.gpg.d/saltstack.gpg

RUN apt-get update -y && apt-get install -y \
    netcat \
    python3-crypto \
    salt-api \
    salt-master \
    python-psycopg2 \
    screen

# Library for SaltMaster manager
COPY src/python/zephir/saltmaster /usr/lib/python3/dist-packages/zephir/saltmaster

# Configuration
COPY master.d/*.conf /etc/salt/master.d/

RUN mkdir /srv/salt
RUN mkdir /srv/pillar
RUN mkdir /keys

# Install salt files
COPY --from=build /srv/formulas/eole-configuration-formula/eole/ /srv/salt/eole/
RUN mkdir /srv/salt/_modules
COPY environment/* /srv/salt/_modules/
COPY pillars/* /srv/pillar

# Controller
RUN mkdir -p /srv/bin
COPY scripts/* /srv/bin/
COPY migrations /migrations
RUN chmod +x /srv/bin/*

# Create the password
ENV SALT_PASSWORD=eole
RUN /srv/bin/create-user.sh
