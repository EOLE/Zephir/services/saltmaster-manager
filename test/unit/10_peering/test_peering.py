from aiohttp import web
import os

from zephir.saltmaster.peering import SaltPeering

def test_salt_peering_generate_keys()

    async def keys(request)
        return web.Response(text="""-----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAww38WjC353e0LuLj7y8l
    DWNDHFTg/b7Afo7+PhZSGjUGxV+70/5qxsshuNOYntm4gfNDMPIsRuinwIwhsACx
    +4OSlpQjC9D7XccWmyavAG2ktKpIt0pQ+N4D0FZdvyEWvULEyo5Ij2ZUWx1/sgAA
    MnOErLQiHBUXdY3QXLpLnKGDOFV7iPAHZ01lY1Rmxpul7ARooQLZ4A8BAa70peyB
    3Mn0kyuOvR0RuY4ntGLyeAvwLLHw+4zNvmuoUL4sGYwXvcLUVkwfU85QFEf1W4+T
    o7wYyX/Djo3movnGEtbKyw+sUkEVovunB57uc5dLsYooRDGYiO/uciOXtOggUa/A
    qwIDAQAB
    -----END PUBLIC KEY-----
    -----BEGIN RSA PRIVATE KEY-----
    MIIEpQIBAAKCAQEAww38WjC353e0LuLj7y8lDWNDHFTg/b7Afo7+PhZSGjUGxV+7
    0/5qxsshuNOYntm4gfNDMPIsRuinwIwhsACx+4OSlpQjC9D7XccWmyavAG2ktKpI
    t0pQ+N4D0FZdvyEWvULEyo5Ij2ZUWx1/sgAAMnOErLQiHBUXdY3QXLpLnKGDOFV7
    iPAHZ01lY1Rmxpul7ARooQLZ4A8BAa70peyB3Mn0kyuOvR0RuY4ntGLyeAvwLLHw
    +4zNvmuoUL4sGYwXvcLUVkwfU85QFEf1W4+To7wYyX/Djo3movnGEtbKyw+sUkEV
    ovunB57uc5dLsYooRDGYiO/uciOXtOggUa/AqwIDAQABAoIBAQCch6+47RnYbi6x
    5bDHdI0Ghl8H8HGyEA9IQRZ02tK+Z0lswVvpT2V+7Oq9+UikSdKQrpiAQggYS4K7
    uXkbViN+1OKhZnYNeIMQicFxdwS/kj24ImNCfCgo33ouCfEnfbVS4q8sYiyGr7cI
    e87LGMcPtbaGJfStlpuB4fGRSIt/QJDRGt85viQqE7CGyqHOVrmwfhTEvnfUbXFi
    74d6VtI0YUVtZp0wiTxQbNQv2MYE8XUn9d68WSmsoS+P23sIP6QZebZaR6SQ1waJ
    FgPQzONa0gGR3l4gV61pxpOxJJ/eq/u31/Zc4P9758+GG9cErRPmQbT6ZMayawmo
    XtltkptBAoGBAPxP4nIWrpzU1HC3IO5oqnKNs9uOkIgF3pPDovcHWHjN15tY8O2E
    Uai8WIY8eJl7HpH8hdxWDJMuEKZ2qSs7XILD3aYy96IJlTD8bNw7X6idBzYP5IdX
    dtTdznHwuNpvxF430dtRBESUyEMEodGXdWZcQQzb/Pi9s/ui2dc+lhpLAoGBAMXn
    2iebjhh1yKPH9IUXRvFWu2rQoYrGLqBrCwUheHe7NmNquDNOe32Ar8oDEJU/90Xf
    ZQ/XVKd4zBH2nFPCUNXhNnRwuXGKyd3SzAdUr3KVotb0su94O9XZJVMcyRFi8Rtu
    xsFi3+CXkpl49cD4nbfgj/PTkc187/AWbyQr6PchAoGATU9o8k5S6IkqIvNp6sM1
    Xu5eaurvDE7IGXy7hXfBuxpPczE4eX6uysYTVeZXiae5AUBf/9h59+sIfQkk4rNI
    sGS1mMRLLmJDTYQ7Euw6bl7XI9CQ1IuPRMBGDpGVApF2mOv/qHTVmvH/vtE/iAoh
    leC0qe0KQWlXKcaHezWZJqsCgYEAgdiwKqKGJYhJ/yN3TEdMdZX5W4jNJJcJCpmQ
    3mnqd2wusCXZ+1e38E0ZiUpfe/0/COocjbszQLoh8ntCcDYjAWFFcDpxgYyVC+R9
    lyW6A00oTS65pCLX52QUeCZ/ydeH5/7TixiLWPk551YUr+qu2HEPIeQjWpZperXN
    cAcbT+ECgYEA6D6ys0ykG5ISrp7isBkmVNMuEzJE6Et5ob3Rq5jtiEHrwmikPBMZ
    tiuLw2HtlNgwG20/8dSVd/IwaPwFaiQ9a/cBys1+hY72U/wtpO2Ef0MWIelAKWQd
    BzSwF5FCoeCL0i7tuPmZovDpH0ulpn+77nVkKWl0r1nT65N5hiZd1qQ=
    -----END RSA PRIVATE KEY-----
    """)

    peering = SaltPeering(None)
    app = web.Application()
    app.router.add_get('/keys', keys)


    result_keys = ("""-----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAww38WjC353e0LuLj7y8l
    DWNDHFTg/b7Afo7+PhZSGjUGxV+70/5qxsshuNOYntm4gfNDMPIsRuinwIwhsACx
    +4OSlpQjC9D7XccWmyavAG2ktKpIt0pQ+N4D0FZdvyEWvULEyo5Ij2ZUWx1/sgAA
    MnOErLQiHBUXdY3QXLpLnKGDOFV7iPAHZ01lY1Rmxpul7ARooQLZ4A8BAa70peyB
    3Mn0kyuOvR0RuY4ntGLyeAvwLLHw+4zNvmuoUL4sGYwXvcLUVkwfU85QFEf1W4+T
    o7wYyX/Djo3movnGEtbKyw+sUkEVovunB57uc5dLsYooRDGYiO/uciOXtOggUa/A
    qwIDAQAB
    -----END PUBLIC KEY-----""",
    """-----BEGIN RSA PRIVATE KEY-----
    Proc-Type: 4,ENCRYPTED
    DEK-Info: DES-EDE3-CBC,70CF10B575E7BFE4

    X91QSLQD70vt/9DqaSUsB6z+wb7i9LgCjP5AQV7ovJKmebyaKoLSOtt4EXrfTyhU
    CP460fg1OsQAfWiDhX2XvSjh1rOFwLs7TDbnqBflbE+c+INX86XB0W0Y80ZefFKj
    Nqs2flOb2/kZiyNL3o3F97mmeU65FNHH26o0XeNXY8nz28t8fG7vF9FLzVWgQotI
    1YMO4VpOd5TQ+q78ABWTgPUNg0SWWkLzZYjnIwIMZR0zyrXGiGaPYP4VD2PfojBf
    suMDt+hhTbf9YQ0mEjMSV4oEdKYSc9yBg4/+wUy+OjDYZces8AMVak5cReB9QZiM
    Qvpeda41aOBUYystLsRACXrHbEvP64MlJQd3ytmdsKvzfjT7Ey7s1vU38p4xkbcL
    mlKSRr9xr6mxDNfkbtNVlpRObBcxXtK45neCKBTkwGaCNUhCyDG4yF11RLtz9Ksv
    SZ4pwn7gRVEXsxVTqseaJ2BPI02LuIjnLr9iNfVylOO2EC61A6/Ek2V+pS9ORZof
    8ygxClAHtuHvyNm8+jN+KxtRVvEvNnDwQZ07gJVFb0H5O8wSyiqtJguCJY01oObc
    PIBrAtC47FDRcJrTWqAiyxlgCfJdhRWQYpcrtEsZtKqeiqcSIYtXio5F+9UwuPTB
    3+sXDkJLT580HKFT6TQyw9PebHcXxhs3fVnB1w86/rqn5EqDoBJaBXug3Ofsh4yE
    1m84TjFvqVTiGpi030yFmezcuJAxIdFshk6whr7wIdmR93b0mDxRS6TJ3BZewQUO
    wkE2F/RXTeq7mUbRtxvkGxpX6FPj82mP2EY0gIEpxNTGRNx3owPDc7pGAi74ZLyf
    lJUhPNL7ZJZYc/c1q/y2pHlA9XAMPIirH8LrOOJIiIvbafMLtu4PKnTf1UyObENy
    LTzZd0ZeKHAZVEBdKtrpxPzdeqJ458X/m7WIiTbvLfIEvid8lipKRMuh5a1SuYLu
    +jHrfnGDo2o1w7+uylhJsrhjsEcpjsoJZjdNkKdJ/RnWM4mLW0hZKxKlNuet17hy
    mrkawH2yvj4Lmg5xQx7rAB7lhNf4H9s1Qn3k5DJWnMD5ir8bbQTQxyToF3W1yGen
    +Vs2Z+RfRSlXTCFprhtMZoie0M+lTbbBgsRTMTBaBZ+RsBk6kVQoMi1awepldmGg
    +Y517YmNTdPAJZf3yMuNtWMRhuWQyAscbBmsgLQaAErwYRX+FiJScaFNz/YJNN3z
    8n+4QZRBO3rR/L6nk5QK4qO5EfmcaMDY7BzXLNW2dAF/xcuBVCMjFC4mLAZobGHC
    vYkip3LZsI0QMh64jQLBO5HUdbtuHltfOU7ho6jLYpHm0mPz/H3hR3VUqs3IbVKV
    ItGPzXHnCCIE6YXyMxEEkC5G+JJddvM1/oxtiXoDzVJl47nH6jFtzgD9uOuCb2tE
    dCiByx9nom782walTnZMNgJqQ/ZH5Zyb+eB/9qsctdsiYpHzBLVTBEjm4p0K4VcL
    qVarL5eae1tKPfajDQBcfCCKaPSRFen7uXSfvq6P40xwn/1Jg6coEH2Bi1jxDuua
    T0i85VzQXH48XQSanrHgudPBhEC54hRCQQCH5bLYFeouI7WVMrQIUE57puoeCqML
    -----END RSA PRIVATE KEY-----""")

    server = await test_server(app)

    try:
        peering.generate_keys()
    except Exception as err:
        assert isinstance(err, SaltPeeringError)

    try:
        peering.generate_keys(passphrase='toto')
    except Exception as err:
        assert isinstance(err, SaltPeeringError)

    try:
        peering.generate_keys(uid='1')
    except Exception as err:
        assert isinstance(err, SaltPeeringError)

    assert peering.generate_keys(uid='1', passphrase='toto') == result_keys


def test_salt_peering_generate_conf()
    master_fingerprint = "ec:1a:b1:12:1e:e7:04:fa:43:80:1c:c7:92:75:81:dd:42:a4:ee:5d:9e:5f:ef:ae:38:8b:de:16:cf:d4:b9:d7"
    def get_master_fingerprint():
        return master_fingerprint

    peering = SaltPeering(None)
    peering.get_master_fingerprint = get_master_fingerprint

    result = "master: {}\nmaster_port: {}\nhash_type: {}\nmaster_finger: {}".format(os.environ['SALTMASTER_PUBLIC_HOST'], 30010, 'sha256', master_fingerprint)
    assert peering.generate_conf == result
