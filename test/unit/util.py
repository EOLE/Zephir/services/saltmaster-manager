from subprocess import run as sp_run
from zephir.database import connect

DB_NAME = 'saltmaster'

DATABASE_OPTIONS = {'host': 'localhost',
                    'dbname': 'postgres',
                    'user': 'postgres',
                    'password': 'mynewpassword'}

def get_database_access():
    conn = connect(None, DATABASE_OPTIONS)
    conn.autocommit = True
    with conn.cursor() as cursor:
        try:
            cursor.execute('DROP DATABASE {};'.format(DB_NAME))
        except:
            pass
        cursor.execute('CREATE DATABASE {};'.format(DB_NAME))
    conn.close()
    database_options = DATABASE_OPTIONS.copy()
    database_options['dbname'] = DB_NAME

    db_uri = "db:postgresql://{}:{}@{}/{}".format(database_options['user'],
                                                  database_options['password'],
                                                  database_options['host'],
                                                  database_options['dbname'])

    sp_run(['sqitch', '--engine', 'postgresql', '--top-dir', '/migrations', 'deploy', db_uri], check=True)
    return connect(None, database_options)


def setup_module(module):
    CONN = get_database_access()
    module.CONN = CONN


def teardown_module(module):
    module.CONN.rollback()
    module.CONN.close()
    conn = connect(None, DATABASE_OPTIONS)
    conn.autocommit = True
    with conn.cursor() as cursor:
        cursor.execute('DROP DATABASE {};'.format(DB_NAME))
    conn.commit()
