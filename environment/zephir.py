# -*- coding: utf-8 -*-
from json import load
from os.path import isfile
import imp

FILE_PATH = '/var/lib/eole/zephir/config.env'
EOSFUNC_PATH = '/var/lib/eole/zephir/config.creolefuncs'

def environment():
    grains = {}
    eosfunc = imp.load_source('eosfunc', EOSFUNC_PATH) 

    if isfile(FILE_PATH):
        with open(FILE_PATH, "r") as fhr:
            probes = load(fhr)
            for name, probe in probes.items():
                grains[name] = getattr(eosfunc, probe["function"])(*probe["args"], **probe["kwargs"])

    return grains
