-- Revert saltmaster:salt-runner-db from pg

BEGIN;

SET ROLE "saltmaster_admin";

-- XXX Add DDLs here.

--
-- Table structure for table `jids`
--
DROP TABLE IF EXISTS jids;

--
-- Table structure for table `salt_returns`
--
DROP TABLE IF EXISTS salt_returns;

COMMIT;
