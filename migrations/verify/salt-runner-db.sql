-- Verify saltmaster:salt-runner-db on pg

BEGIN;

SET ROLE "saltmaster_admin";

-- XXX Add verifications here.

-- Limit verification to existence of tables
SELECT jid from jids LIMIT 1;
SELECT jid from salt_returns LIMIT 1;
SELECT id from salt_events LIMIT 1;

ROLLBACK;
