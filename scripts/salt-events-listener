#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
import time

from autobahn.asyncio.wamp import ApplicationSession
from autobahn.asyncio.wamp import ApplicationRunner
from autobahn.wamp.types import SubscribeOptions
from autobahn.wamp.uri import convert_starred_uri
from autobahn import wamp

from zephir.config import ServiceConfig

class SaltEventsListener(ApplicationSession):

    async def onJoin(self, details):

        print("Salt events listener: crossbar joined.")
        # subscribe all methods on this object decorated with "@wamp.subscribe"
        # as PubSub event handlers
        results = await self.subscribe(self)
        for res in results:
            if isinstance(res, wamp.protocol.Subscription):
                # res is an Subscription instance
                msg = "Salt events listener: topic “{}” subscribed by handler “{}” with subscription ID “{}”"
                if res.handler.obj:
                    handler_name = '{}.{}'.format(res.handler.obj.__class__.__name__, res.handler.fn.__name__)
                else:
                    handler_name = res.handler.fn.__name__
                print(msg.format(res.topic, handler_name, res.id))
            else:
                # res is an Failure instance
                print('Salt events listener: failed to subscribe topic: {}'.format(res))

    @wamp.subscribe('v1.execution.salt.exec.command-executed')
    def ExecutedCommand(self, *args, **kwargs):
        msg = 'Got “v1.execution.salt.exec.command-executed” event with arguments “{}”'
        print(msg.format(kwargs))

    @wamp.subscribe('v1.execution.salt.peer.registered')
    def PeerRegistered(self, *args, **kwargs):
        msg = 'Got "v1.execution.salt.peer.registered" event with arguments "{}"'
        print(msg.format(kwargs))

    @wamp.subscribe('v1.storage.event')
    def StorageEvent(self, *args, **kwargs):
        msg = 'Got "v1.storage.event" event with arguments "{}"'
        print(msg.format(kwargs))

    @wamp.subscribe('v1.server.created')
    def ServerCreated(self, *args, **kwargs):
        msg = 'Got "v1.server.created" event with arguments "{}"'
        print(msg.format(kwargs))

    @wamp.subscribe('v1.passphrase.issued')
    def PassphraseIssued(self, *args, **kwargs):
        msg = 'Got "v1.passphrase.issued" event with arguments "{}"'
        print(msg.format(kwargs))


if __name__ == '__main__':
    config = ServiceConfig()
    config.load_config()

    # Configuration file does not exists -> wait
    while (config.option is None
           or not config.option['crossbar']['host']
           or not config.option['crossbar']['realm']):
        print('Salt events listener: configuration file unavailable. Waiting...')
        time.sleep(5)
        config.load_config()

    # initialize WAMP session
    runner = ApplicationRunner(
        config.option['crossbar']['host'],
        config.option['crossbar']['realm'],
    )
    runner.run(SaltEventsListener)
