#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
import signal
import time
import json, yaml
from os import path, makedirs, getpid, remove, walk, path, stat, rename
from os.path import isfile
from shutil import rmtree
import tarfile
import base64

from autobahn.wamp.exception import ApplicationError

from zephir.controller import ZephirCommonController, run
from zephir.http import register as register_http
from zephir.wamp import register as register_wamp
from zephir.coroutine import register as register_coroutine

from zephir.saltmaster.execute import SaltExec
from zephir.saltmaster.execute import SaltExecErrorMissingMinionPattern
from zephir.saltmaster.execute import SaltExecErrorMissingCommand
from zephir.saltmaster.execute import SaltExecErrorLoginError
from zephir.saltmaster.execute import SaltExecErrorExecuteError

from zephir.saltmaster.jobs import SaltJobs
from zephir.saltmaster.jobs import SaltJobsErrorDbConnectionError
from zephir.saltmaster.jobs import SaltJobsErrorListError
from zephir.saltmaster.jobs import SaltJobsErrorMissingJobId
from zephir.saltmaster.jobs import SaltJobsErrorJobIdTypeError
from zephir.saltmaster.jobs import SaltJobsErrorUnknownJobId

from zephir.saltmaster.peering import SaltPeering
from zephir.saltmaster.events import SaltEvents


MINION_DIR = '/srv/salt/eole/configuration/files/'


class SaltRunner(ZephirCommonController):
    """Salt controller based on salt-api

    """

    def __init__(self, *args, **kwargs):
        # Initialise self.config
        super().__init__(*args, **kwargs)

        # Create instance of working code
        self.executor = SaltExec(config=self.config.extra)
        self.jobs = SaltJobs(config=self.config.extra)
        self.peers = SaltPeering(config=self.config.extra)
        self.events = SaltEvents(config=self.config.extra, wamp=self)


    @register_wamp('v1.execution.salt.exec', notification_uri='v1.execution.salt.exec.command-executed')
    async def exec_command(self, minion_pattern, command, arg, client_mode, tgt_type):
        try:
            ret = self.executor.exec_command(minion_pattern, command, arg, client_mode, tgt_type)
            return ret

        except SaltExecErrorMissingMinionPattern as err:
            raise ApplicationError('execution.salt.exec.error.missing-minion_pattern', reason=str(err))

        except SaltExecErrorMissingCommand as err:
            raise ApplicationError(u'execution.salt.exec.error.missing-command', reason=str(err))

        except SaltExecErrorLoginError as err:
            raise ApplicationError('execution.salt.exec.error.login', reason=str(err))

        except SaltExecErrorExecuteError as err:
            raise ApplicationError('execution.salt.exec.error.execute', reason=str(err))


    @register_wamp('v1.execution.salt.job.list', notification_uri=None, database=True)
    async def list_jobs(self, cursor, minion_pattern):
        try:
            return list(self.jobs.list_jobs(cursor, minion_pattern))

        except SaltJobsErrorDbConnectionError as err:
            raise ApplicationError('execution.salt.job.error.db-connection', reason=str(err))

        except SaltJobsErrorListError as err:
            raise ApplicationError('execution.salt.job.error.list', reason=str(err))


    @register_wamp('v1.execution.salt.job.describe', notification_uri=None, database=True)
    async def describe_job(self, cursor, jid):
        try:
            return self.jobs.describe_job(cursor, jid)

        except SaltJobsErrorDbConnectionError as err:
            raise ApplicationError('execution.salt.job.error.db-connection', reason=str(err))

        except SaltJobsErrorMissingJobId as err:
            raise ApplicationError('execution.salt.job.error.missing-jid', reason=str(err))

        except SaltJobsErrorJobIdTypeError as err:
            raise ApplicationError('execution.salt.job.error.jid-type', reason=str(err))

        except SaltJobsErrorUnknownJobId as err:
            raise ApplicationError('execution.salt.job.error.unknown-jid', reason=str(err))

    def pillar_clear_cache(self, minion_pattern):
        self.executor.exec_command(minion_pattern=minion_pattern,
                                   command='saltutil.clear_cache')
        self.executor.exec_command(minion_pattern=minion_pattern,
                                   command='saltutil.sync_all',
                                   client_mode="local")

    @register_wamp('v1.server.configuration.updated', None)
    async def configuration_update(self, servers_id):
        for server_id in servers_id:
            server = await self.call('v1.server.describe',
                                     serverid=server_id,
                                     configuration=True)

            pillar_file_path = '/srv/pillar/pillars_{0}.sls'.format(server_id)
            with open(pillar_file_path + '.tmp', 'w') as fh:
                if json.loads(server['configuration']):
                    yaml.dump(json.loads(server['configuration']), fh, allow_unicode=True)
                else:
                    yaml.dump({}, fh)
            if isfile(pillar_file_path):
                remove(pillar_file_path)
            rename(pillar_file_path + '.tmp', pillar_file_path)

            self.pillar_clear_cache(str(server_id))

    @register_wamp('v1.execution.salt.configuration.deploy',
                   notification_uri='v1.execution.salt.configuration.deploy-scheduled')
    async def configuration_deploy(self, minion_pattern):
        await self.download_configuration_files(int(minion_pattern))

        try:
            deploy_command = 'state.apply'
            deploy_arg = 'eole.configuration.deploy'
            ret = self.executor.exec_command(minion_pattern=minion_pattern,
                                             command=deploy_command,
                                             arg=deploy_arg)
            self.pillar_clear_cache(minion_pattern)
            return ret

        except SaltExecErrorMissingMinionPattern as err:
            raise ApplicationError('execution.salt.exec.error.missing-minion_pattern', reason=str(err))

        except SaltExecErrorLoginError as err:
            raise ApplicationError('execution.salt.exec.error.login', reason=str(err))

        except SaltExecErrorExecuteError as err:
            raise ApplicationError('execution.salt.exec.error.execute', reason=str(err))

    async def server_started(self, server_id):
        await self.download_env(server_id)
        self.executor.exec_command(minion_pattern=str(server_id),
                                   command='state.apply',
                                   arg='eole.configuration.deploy')
        # FIXME refresh = true
        self.executor.exec_command(minion_pattern=str(server_id),
                                   command='saltutil.sync_all',
                                   client_mode="local")

    @register_wamp('v1.execution.salt.master.event.start', notification_uri='v1.execution.salt.master.event.ready')
    async def minion_start(self, server_id):
        await self.server_started(server_id)
        await self.call('v1.server.peer-connection.update',
                        serverid=server_id)
        return {'server_id': server_id}

    @register_wamp('v1.execution.salt.environment.get', notification_uri=None)
    async def get_environment(self, server_id):
        server_id = str(server_id)
        self.executor.exec_command(minion_pattern=str(server_id),
                                   command='saltutil.sync_all',
                                   client_mode="local")
        return self.executor.exec_command(server_id,
                                          'zephir.environment',
                                          None,
                                          'local')[server_id]

    async def download_configuration_files(self,
                                           server_id):
        minion_dir = '{0}{1}'.format(MINION_DIR, server_id)
        creolefuncs_file_path = '{0}/config.creolefuncs'.format(minion_dir)
        pillar_file_path = '/srv/pillar/pillars_files_{0}.sls'.format(server_id)
        creole_files_path = '{0}/creole_files.tar.gz'.format(minion_dir)

        server = await self.call('v1.server.describe',
                                 serverid=server_id)

        servermodel_id = server['servermodelid']
        del server

        servermodel = await self.call('v1.servermodel.describe',
                                      servermodelid=servermodel_id,
                                      inheritance=False,
                                      resolvdepends=False,
                                      creolefuncs=True,
                                      probes=True,
                                      conffiles=True)

        if path.isdir(minion_dir):
            rmtree(minion_dir)
        makedirs(minion_dir)

        self._write_file(creolefuncs_file_path, servermodel['creolefuncs'])

        eoleconfig = {"eole":{"configuration": { "files":{}}}}

        with open(creole_files_path, 'wb') as fh:
            fh.write(base64.b64decode(servermodel['conffiles']))
            tar = tarfile.open(creole_files_path,'r:gz')
            tar.extractall(minion_dir)
            remove(creole_files_path)

            # r=root, d=directories, f = files
            for r, d, f in walk(f'{minion_dir}/creole_files'):
                for file in f:
                    mode = oct(stat(path.join(r, file)).st_mode)[-3:]
                    pathtofile = path.join(r.replace(f'{minion_dir}/creole_files', ''), file)
                    eoleconfig['eole']['configuration']['files'][pathtofile] = {}
                    eoleconfig['eole']['configuration']['files'][pathtofile]['owner'] = 'root'
                    eoleconfig['eole']['configuration']['files'][pathtofile]['group'] = 'root'
                    eoleconfig['eole']['configuration']['files'][pathtofile]['mode'] = mode
        # if json.loads(server['configuration']):
        self._write_file(pillar_file_path, yaml.dump(eoleconfig, default_flow_style=False))
        self._download_env(server_id, servermodel)

    def _download_env(self, server_id, servermodel):
        minion_dir = '{0}{1}'.format(MINION_DIR, server_id)
        makedirs(minion_dir, exist_ok=True)
        env_file_path = '{0}/config.env'.format(minion_dir)

        self._write_file(env_file_path, servermodel['probes'])

    def _write_file(self, filename, content):
        with open(filename + '.tmp', 'w') as fh:
            fh.write(content)
        if isfile(filename):
            remove(filename)
        rename(filename + '.tmp', filename)

    async def download_env(self,server_id):
        server = await self.call('v1.server.describe',
                                 serverid=server_id)
        servermodel = await self.call('v1.servermodel.describe',
                                      servermodelid=server['servermodelid'],
                                      inheritance=False,
                                      resolvdepends=False,
                                      schema=False,
                                      probes=True)
        self._download_env(server_id, servermodel)

    @register_wamp('v1.server.created', notification_uri='v1.execution.salt.peer.registered')
    async def peer_register(self, serverid):
        try:
            await self.download_configuration_files(serverid)
            secret = await self.call('v1.vault.secret.get', secretkey="{}_passphrase".format(serverid))
            passphrase = secret["secret"]["passphrase"]
            minion_id = str(serverid)
            minion_conf = await self.peers.generate_conf()
            pub_key, ciphered_priv_key = await self.peers.generate_keys(minion_id, passphrase)
            secrets = {"serverid": serverid,
                       "client_configuration": minion_conf,
                       "pub_key": pub_key,
                       "ciphered_priv_key": ciphered_priv_key.decode()}
            await self.call('v1.vault.secret.set', secretkey="{}_peeringconf".format(serverid), secret=secrets)
            pillar_conf_path = '/srv/pillar/pillars_{0}.sls'.format(serverid)
            pillar_file_path = '/srv/pillar/pillars_files_{0}.sls'.format(serverid)
            with open(pillar_file_path, 'w'):
                pass
            with open(pillar_conf_path, 'w'):
                pass
            return {"serverid": serverid, "automation": "salt"}
        except Exception as err:
            import traceback
            traceback.print_exc()
            raise ApplicationError('execution.salt.peer.error', reason=str(err))

    @register_coroutine(as_thread=True)
    def forward_saltmaster_events(self):
      print("forwarding saltmaster events as wamp messages")
      self.events.forward_events()


if __name__ == '__main__':
    run(SaltRunner)
